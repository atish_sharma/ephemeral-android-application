# Ephemeral Android Application #

### Purpose ###

Ephemeral is a prototype android application which was developed for INK Makers Make-a-thon 2.0‏ hosted in MIT Manipal. This application allows its users to create virtual "boards". The users can draw on these "boards" and these changes will be reflected in real time for any other user who is viewing the same board.

### Scope ###

The users of this application can use the virtual boards as a medium to share ideas and collaborate with each other in real time as this application offers a minimal clean UI which is interactive and real time in nature.

### Operating Environment ###

  1. Android OS 4.0 or greater
  2. Working internet connection