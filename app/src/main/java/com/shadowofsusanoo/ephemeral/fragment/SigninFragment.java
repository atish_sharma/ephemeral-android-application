package com.shadowofsusanoo.ephemeral.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shadowofsusanoo.ephemeral.R;

public class SigninFragment extends Fragment {

    AppCompatActivity activity;
    TextView login,signup;

    public SigninFragment() {}

    public static SigninFragment getInstance(AppCompatActivity activity){
        SigninFragment fragment = new SigninFragment();
        fragment.activity=activity;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_signin, container, false);
        setFrame(LoginFragment.getInstance(activity));
        login=(TextView)root.findViewById(R.id.login);
        signup=(TextView)root.findViewById(R.id.signup);
        login.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                setFrame(LoginFragment.getInstance(activity));
            }
        });
        signup.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setFrame(SignupFragment.getInstance(activity));
            }
        });
        return root;
    }

    void setFrame(Fragment fragment){
        FragmentTransaction fragmentTransaction=activity.getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.signin_frame,fragment);
        fragmentTransaction.commit();
    }

}
