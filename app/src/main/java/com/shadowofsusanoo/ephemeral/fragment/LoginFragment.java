package com.shadowofsusanoo.ephemeral.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.shadowofsusanoo.ephemeral.R;
import com.shadowofsusanoo.ephemeral.activity.MainActivity;

public class LoginFragment extends Fragment {

    AppCompatActivity activity;
    TextView username,password;
    Button clear,submit;
    ProgressBar progressBar;

    public LoginFragment() {}

    public static LoginFragment getInstance(AppCompatActivity activity){
        LoginFragment fragment = new LoginFragment();
        fragment.activity=activity;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_login, container, false);
        username=(TextView)root.findViewById(R.id.username);
        password=(TextView)root.findViewById(R.id.password);
        clear=(Button)root.findViewById(R.id.clear);
        submit=(Button)root.findViewById(R.id.submit);
        progressBar=(ProgressBar)root.findViewById(R.id.progress_bar);
        clear.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                username.setText("");
                password.setText("");
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = username.getText().toString().trim().toLowerCase();
                String pass = password.getText().toString().trim();
                String error="";
                if(user.equals("")){
                    if(!error.equals(""))error+="\n";
                    error+="Enter username.";
                }
                if(pass.equals("")){
                    if(!error.equals(""))error+="\n";
                    error+="Enter password.";
                }
                if(error.equals("")) {
                    progressBar.setVisibility(View.VISIBLE);
                    ParseUser.logInInBackground(user, pass, new LogInCallback() {
                        @Override
                        public void done(ParseUser user, ParseException e) {
                            if(user!=null){
                                user.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if(e==null){
                                            ((MainActivity)activity).setFrame(HomeFragment.getInstance(activity));
                                        }else{
                                            Toast.makeText(activity,"Something went wrong! Please try again.",Toast.LENGTH_SHORT).show();
                                        }
                                        progressBar.setVisibility(View.GONE);
                                    }
                                });
                            }else{
                                e.printStackTrace();
                                String error="";
                                switch(e.getCode()){
                                    case ParseException.OBJECT_NOT_FOUND:
                                        error="Invalid username/password combination.";
                                        break;
                                    default:
                                        error="Something went wrong! Please try again.";
                                }
                                Toast.makeText(activity,error,Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                            }
                        }
                    });
                }else{
                    Toast.makeText(activity,error,Toast.LENGTH_SHORT).show();
                }
            }
        });
        return root;
    }

}