package com.shadowofsusanoo.ephemeral.fragment;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.shadowofsusanoo.ephemeral.R;
import com.shadowofsusanoo.ephemeral.activity.MainActivity;
import com.shadowofsusanoo.ephemeral.adapter.TabsPagerAdapter;

public class HomeFragment extends Fragment {

    AppCompatActivity activity;

    public HomeFragment() {}

    public static HomeFragment getInstance(AppCompatActivity activity){
        HomeFragment fragment = new HomeFragment();
        fragment.activity=activity;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_home, container, false);
        ((MainActivity)activity).setMenuItemsVisibility(true);
        TabLayout tabLayout = (TabLayout) root.findViewById(R.id.tab_layout);
        final String[] title={"My Boards","Search"};
        final int[] icons={R.mipmap.ic_developer_board_white_24dp,R.mipmap.ic_search_white_24dp};
        for(int i=0;i<title.length;i++) {
            //tabLayout.addTab(tabLayout.newTab().setText(title[i]));
            tabLayout.addTab(tabLayout.newTab().setIcon(icons[i]));
        }
        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
        final ViewPager viewPager = (ViewPager) root.findViewById(R.id.pager);
        final TabsPagerAdapter adapter = new TabsPagerAdapter(activity.getSupportFragmentManager(), activity);
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                int tabPosition = tab.getPosition();
                viewPager.setCurrentItem(tabPosition);
                activity.getSupportActionBar().setTitle(title[tabPosition]);
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {}

            @Override
            public void onTabReselected(TabLayout.Tab tab) {}

        });
        viewPager.setCurrentItem(0);
        activity.getSupportActionBar().setTitle(title[0]);
        return root;
    }

}
