package com.shadowofsusanoo.ephemeral.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.shadowofsusanoo.ephemeral.activity.DrawingActivity;
import com.shadowofsusanoo.ephemeral.R;
import com.shadowofsusanoo.ephemeral.adapter.BoardAdapter;
import com.shadowofsusanoo.ephemeral.key.Key;

import java.util.ArrayList;
import java.util.List;

public class SearchFragment extends Fragment {

    AppCompatActivity activity;
    ListView list;
    BoardAdapter adapter;
    ArrayList<ParseObject> boards;
    ProgressBar progressBar;
    EditText searchTerm;
    Button search;

    public SearchFragment() {}

    public static SearchFragment getInstance(AppCompatActivity activity){
        SearchFragment fragment = new SearchFragment();
        fragment.activity=activity;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_search, container, false);
        View listHeader = activity.getLayoutInflater().inflate(R.layout.header_board_list, null);
        View listFooter = activity.getLayoutInflater().inflate(R.layout.footer_board_list, null);
        boards=new ArrayList<>();
        adapter=new BoardAdapter(activity,boards);
        progressBar=(ProgressBar)root.findViewById(R.id.progress_bar);
        list=(ListView)root.findViewById(R.id.list_boards);
        search=(Button)root.findViewById(R.id.search);
        searchTerm=(EditText)root.findViewById(R.id.search_text);
        list.addHeaderView(listHeader);
        list.addFooterView(listFooter);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                try {
                    ParseObject po = boards.get(position - 1);
                    Intent i = new Intent(activity, DrawingActivity.class);
                    i.putExtra(Key.DRAWING_ID, po.getObjectId());
                    startActivity(i);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
        search.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                String term=searchTerm.getText().toString().trim().toLowerCase();
                if(term.equals("")){
                    Toast.makeText(activity,"Please enter a search term.",Toast.LENGTH_SHORT).show();
                }else{
                    downloadBoards(term);
                }
            }
        });
        return root;
    }

    void downloadBoards(String term){
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Key.PARSE_OBJECT_BOARD);
        query.whereNotEqualTo(Key.PARSE_BOARD_OWNER, ParseUser.getCurrentUser());
        query.whereContains(Key.PARSE_BOARD_TITLE, term);
        progressBar.setVisibility(View.VISIBLE);
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    boards.clear();
                    for(int i=0;i<objects.size();i++)boards.add(objects.get(i));
                    adapter.notifyDataSetChanged();
                } else {
                    e.printStackTrace();
                    Toast.makeText(activity, "Unable to fetch boards! Please try again.", Toast.LENGTH_SHORT).show();
                }
                progressBar.setVisibility(View.GONE);
            }
        });
    }

}
