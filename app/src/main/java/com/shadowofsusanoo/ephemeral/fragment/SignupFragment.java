package com.shadowofsusanoo.ephemeral.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.parse.ParseException;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.SignUpCallback;
import com.shadowofsusanoo.ephemeral.R;
import com.shadowofsusanoo.ephemeral.activity.MainActivity;
import com.shadowofsusanoo.ephemeral.key.Key;
import com.shadowofsusanoo.ephemeral.utility.Utility;

public class SignupFragment extends Fragment {

    AppCompatActivity activity;
    TextView username,password,name;
    Button clear,submit;
    ProgressBar progressBar;

    public SignupFragment() {}

    public static SignupFragment getInstance(AppCompatActivity activity){
        SignupFragment fragment = new SignupFragment();
        fragment.activity=activity;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_signup, container, false);
        username=(TextView)root.findViewById(R.id.username);
        password=(TextView)root.findViewById(R.id.password);
        name=(TextView)root.findViewById(R.id.full_name);
        clear=(Button)root.findViewById(R.id.clear);
        submit=(Button)root.findViewById(R.id.submit);
        progressBar=(ProgressBar)root.findViewById(R.id.progress_bar);
        clear.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                username.setText("");
                password.setText("");
                name.setText("");
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String user = username.getText().toString().trim().toLowerCase();
                String pass = password.getText().toString().trim();
                String f_name = Utility.cFirst(name.getText().toString().trim().toLowerCase());
                String error="";
                if(user.equals("")){
                    if(!error.equals(""))error+="\n";
                    error+="Enter username.";
                }
                if(f_name.equals("")){
                    if(!error.equals(""))error+="\n";
                    error+="Enter full name.";
                }
                if(pass.equals("")){
                    if(!error.equals(""))error+="\n";
                    error+="Enter password.";
                }
                if(error.equals("")){
                    final ParseUser parseUser=new ParseUser();
                    parseUser.setUsername(user);
                    parseUser.setPassword(pass);
                    parseUser.put(Key.PARSE_USER_NAME, f_name);
                    progressBar.setVisibility(View.VISIBLE);
                    parseUser.signUpInBackground(new SignUpCallback() {
                        @Override
                        public void done(ParseException e) {
                            if(e==null){
                                parseUser.saveInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if(e==null){
                                            ((MainActivity)activity).setFrame(HomeFragment.getInstance(activity));
                                        }else{
                                            Toast.makeText(activity,"Something went wrong! Please try again.",Toast.LENGTH_SHORT).show();
                                        }
                                        progressBar.setVisibility(View.GONE);
                                    }
                                });
                            }else{
                                String error="";
                                e.printStackTrace();
                                switch (e.getCode()) {
                                    case ParseException.USERNAME_TAKEN:
                                        error="Username already in use!";
                                        break;
                                    default:
                                        error="Something went wrong! Please try again.";
                                }
                                Toast.makeText(activity,error,Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                            }
                        }
                    });
                }else{
                    Toast.makeText(activity,error,Toast.LENGTH_SHORT).show();
                }
            }
        });
        return root;
    }

}