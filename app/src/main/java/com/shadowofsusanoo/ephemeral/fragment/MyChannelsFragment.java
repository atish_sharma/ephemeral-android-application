package com.shadowofsusanoo.ephemeral.fragment;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.getbase.floatingactionbutton.FloatingActionButton;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.shadowofsusanoo.ephemeral.activity.DrawingActivity;
import com.shadowofsusanoo.ephemeral.R;
import com.shadowofsusanoo.ephemeral.adapter.BoardAdapter;
import com.shadowofsusanoo.ephemeral.builder.AddBoardDialogBuilder;
import com.shadowofsusanoo.ephemeral.key.Key;

import java.util.ArrayList;
import java.util.List;

public class MyChannelsFragment extends Fragment implements SwipeRefreshLayout.OnRefreshListener{

    AppCompatActivity activity;
    FloatingActionButton addBoard;
    SwipeRefreshLayout refresh;
    ListView list;
    BoardAdapter adapter;
    ArrayList<ParseObject> boards;
    RelativeLayout noBoards;

    public MyChannelsFragment() {}

    public static MyChannelsFragment getInstance(AppCompatActivity activity){
        MyChannelsFragment fragment = new MyChannelsFragment();
        fragment.activity=activity;
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_my_channels, container, false);
        View listHeader = activity.getLayoutInflater().inflate(R.layout.header_board_list, null);
        View listFooter = activity.getLayoutInflater().inflate(R.layout.footer_board_list, null);
        boards=new ArrayList<>();
        adapter=new BoardAdapter(activity,boards);
        list=(ListView)root.findViewById(R.id.list_boards);
        noBoards=(RelativeLayout)root.findViewById(R.id.no_boards);
        addBoard=(FloatingActionButton)root.findViewById(R.id.add_board);
        addBoard.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View v){
                AddBoardDialogBuilder.getDialog(activity,MyChannelsFragment.this).show();
            }
        });
        refresh = (SwipeRefreshLayout) root.findViewById(R.id.refresh_boards);
        refresh.setOnRefreshListener(this);
        refresh.setColorSchemeResources(
                R.color.primary,
                R.color.primary_dark,
                R.color.accent
        );
        list.addHeaderView(listHeader);
        list.addFooterView(listFooter);
        list.setAdapter(adapter);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3) {
                try {
                    ParseObject po = boards.get(position - 1);
                    Intent i = new Intent(activity, DrawingActivity.class);
                    i.putExtra(Key.DRAWING_ID, po.getObjectId());
                    startActivity(i);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }
        });
        reloadBoardsFromLocalDatastore();
        return root;
    }

    void downloadBoards(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Key.PARSE_OBJECT_BOARD);
        query.whereEqualTo(Key.PARSE_BOARD_OWNER, ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if(e==null){
                    ParseObject.pinAllInBackground(objects, new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if(e==null){
                                reloadBoardsFromLocalDatastore();
                            }else{
                                e.printStackTrace();
                                Toast.makeText(activity,"Unable to fetch boards! Please try again.",Toast.LENGTH_SHORT).show();
                            }
                            refresh.setRefreshing(false);
                        }
                    });
                }else{
                    e.printStackTrace();
                    Toast.makeText(activity,"Unable to fetch boards! Please try again.",Toast.LENGTH_SHORT).show();
                    refresh.setRefreshing(false);
                }
            }
        });
    }

    public void reloadBoardsFromLocalDatastore(){
        ParseQuery<ParseObject> query = ParseQuery.getQuery(Key.PARSE_OBJECT_BOARD);
        query.fromLocalDatastore();
        query.whereEqualTo(Key.PARSE_BOARD_OWNER, ParseUser.getCurrentUser());
        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e == null) {
                    boards.clear();
                    for(int i=0;i<objects.size();i++)boards.add(objects.get(i));
                    if(boards.size()==0){
                        noBoards.setVisibility(View.VISIBLE);
                    }else{
                        noBoards.setVisibility(View.GONE);
                    }
                    adapter.notifyDataSetChanged();
                } else {
                    e.printStackTrace();
                    Toast.makeText(activity, "Unable to fetch boards! Please try again.", Toast.LENGTH_SHORT).show();
                    refresh.setRefreshing(false);
                }
            }
        });
    }

    @Override
    public void onRefresh() {
        downloadBoards();
    }

}
