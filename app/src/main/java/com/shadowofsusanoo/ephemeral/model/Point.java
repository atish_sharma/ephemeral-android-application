package com.shadowofsusanoo.ephemeral.model;

/**
 * Created by atish on 30/1/16.
 */
public class Point{

    private float x,y;

    public Point(float x,float y){
        this.x=x;
        this.y=y;
    }

    public float getX(){
        return x;
    }

    public float getY(){
        return y;
    }

    public Point convertToStandard(Point oldPoint,float oldWidth,float oldHeight,float newWidth,float newHeight){
        float newX,newY;
        newX = oldPoint.getX()*(newWidth/oldWidth);
        newY = oldPoint.getY()*(newHeight/oldHeight);
        return new Point(newX,newY);
    }


}
