package com.shadowofsusanoo.ephemeral.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseUser;
import com.shadowofsusanoo.ephemeral.R;
import com.shadowofsusanoo.ephemeral.fragment.HomeFragment;
import com.shadowofsusanoo.ephemeral.fragment.SigninFragment;


public class MainActivity extends AppCompatActivity {

    boolean showMenu;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        try {
            Parse.enableLocalDatastore(this);
        }catch(Exception e){
            e.printStackTrace();
        }
        try {
            Parse.initialize(this);
        }catch(Exception e){
            e.printStackTrace();
        }
        showMenu=false;
        if(ParseUser.getCurrentUser()==null){
            setFrame(SigninFragment.getInstance(this));
        }else{
            setFrame(HomeFragment.getInstance(this));
        }
    }

    public void setFrame(Fragment fragment){
        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.main_frame,fragment);
        fragmentTransaction.commit();
    }

    public void setMenuItemsVisibility(boolean visible){
        showMenu=visible;
        invalidateOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        int arr[]={R.id.action_profile};
        for(int i=0;i<arr.length;i++){
            MenuItem item=menu.findItem(arr[i]);
            item.setVisible(showMenu);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch(id){
            case R.id.action_profile:
                intent=new Intent(this,ProfileActivity.class);
                startActivity(intent);
        }
        return super.onOptionsItemSelected(item);
    }
}
