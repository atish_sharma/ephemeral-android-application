package com.shadowofsusanoo.ephemeral.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.shadowofsusanoo.ephemeral.R;
import com.shadowofsusanoo.ephemeral.key.Key;

public class ProfileActivity extends AppCompatActivity {

    Button logout;
    TextView username,name;
    ParseUser user;
    ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        logout=(Button)findViewById(R.id.logout);
        username=(TextView)findViewById(R.id.username);
        name=(TextView)findViewById(R.id.full_name);
        progressBar=(ProgressBar)findViewById(R.id.progress_bar);
        user=ParseUser.getCurrentUser();
        username.setText(user.getUsername());
        name.setText(user.getString(Key.PARSE_USER_NAME));
        logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                progressBar.setVisibility(View.VISIBLE);
                user.logOutInBackground(new LogOutCallback() {
                    @Override
                    public void done(ParseException e) {
                        if(e!=null){
                            e.printStackTrace();
                            Toast.makeText(getBaseContext(),"Unable to log you out! Pleas try again.",Toast.LENGTH_SHORT).show();
                        }else{
                            Toast.makeText(getBaseContext(),"You have been successfully logged out.",Toast.LENGTH_SHORT).show();
                            Intent i = new Intent(ProfileActivity.this,MainActivity.class);
                            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                            ParseObject.unpinAllInBackground();
                            startActivity(i);
                        }
                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        });
    }

}
