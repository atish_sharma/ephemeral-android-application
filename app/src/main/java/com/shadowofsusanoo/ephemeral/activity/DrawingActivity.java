package com.shadowofsusanoo.ephemeral.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import com.firebase.client.Firebase;
import com.getbase.floatingactionbutton.FloatingActionButton;
import com.shadowofsusanoo.ephemeral.R;
import com.shadowofsusanoo.ephemeral.canvas.DrawingCanvas;
import com.shadowofsusanoo.ephemeral.key.Key;


public class DrawingActivity extends AppCompatActivity {

    String boardId;
    FloatingActionButton close;
    AppCompatActivity activity;
    DrawingCanvas canvas;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN, WindowManager.LayoutParams.FLAG_FULLSCREEN);
        }catch(Exception e){
            e.printStackTrace();
        }
        super.onCreate(savedInstanceState);
        Firebase.setAndroidContext(this);
        setContentView(R.layout.activity_drawing);
        try {
            getSupportActionBar().hide();
        }catch(Exception e){
            e.printStackTrace();
        }
        boardId=getIntent().getStringExtra(Key.DRAWING_ID);
        activity=this;
        close=(FloatingActionButton)findViewById(R.id.close);
        canvas=(DrawingCanvas)findViewById(R.id.canvas);
        canvas.setFirebaseReference(boardId,"https://ephemeral404.firebaseio.com/");
        close.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                canvas.clearCanvas();
            }
        });
    }

}
