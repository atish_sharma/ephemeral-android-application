package com.shadowofsusanoo.ephemeral.key;

/**
 * Created by atish on 28/1/16.
 */
public class Key {

    public static final String PARSE_USER_NAME="full_name";

    public static final String PARSE_OBJECT_BOARD="Board";
    public static final String PARSE_BOARD_OWNER="board_owner";
    public static final String PARSE_BOARD_TITLE="board_title";
    public static final String PARSE_BOARD_DESCRIPTION="board_description";

    public static final String DRAWING_ID="drawing id";

}
