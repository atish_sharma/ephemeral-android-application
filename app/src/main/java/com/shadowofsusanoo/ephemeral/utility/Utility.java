package com.shadowofsusanoo.ephemeral.utility;

/**
 * Created by atish on 29/1/16.
 */
public class Utility {

    public static String cFirst(String a){
        try {
            String[] t = a.split(" ");
            String o = "";
            for (int i = 0; i < t.length; i++) {
                String temp = t[i].toLowerCase();
                StringBuilder sb = new StringBuilder(temp);
                if (temp.length() > 0) {
                    sb.setCharAt(0, Character.toUpperCase(temp.charAt(0)));
                }
                o = o + " " + sb.toString();
            }
            return o.trim();
        }catch(Exception e){
            e.printStackTrace();
        }
        return a;
    }

}
