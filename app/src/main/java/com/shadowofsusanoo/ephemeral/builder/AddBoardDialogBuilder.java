package com.shadowofsusanoo.ephemeral.builder;

import android.app.Dialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.shadowofsusanoo.ephemeral.R;
import com.shadowofsusanoo.ephemeral.fragment.MyChannelsFragment;
import com.shadowofsusanoo.ephemeral.key.Key;

/**
 * Created by atish on 29/1/16.
 */
public class AddBoardDialogBuilder {

    public static Dialog getDialog(final AppCompatActivity activity, final MyChannelsFragment myChannels){
        final Dialog dialog = new Dialog(activity);
        dialog.setContentView(R.layout.dialog_new_board);
        dialog.setTitle("New Board");
        final ProgressBar progressBar=(ProgressBar)dialog.findViewById(R.id.progress_bar);
        final Button cancel=(Button)dialog.findViewById(R.id.cancel);
        final Button submit=(Button)dialog.findViewById(R.id.submit);
        final EditText title=(EditText)dialog.findViewById(R.id.title);
        final EditText description=(EditText)dialog.findViewById(R.id.description);
        cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String t= title.getText().toString().trim().toLowerCase();
                String desc=description.getText().toString().trim();
                String error="";
                if(t.equals("")){
                    if(!error.equals(""))error+="\n";
                    error+="Enter title.";
                }
                if(desc.equals("")){
                    if(!error.equals(""))error+="\n";
                    error+="Enter description.";
                }
                if(error.equals("")){
                    final ParseObject channel=new ParseObject(Key.PARSE_OBJECT_BOARD);
                    channel.put(Key.PARSE_BOARD_OWNER,ParseUser.getCurrentUser());
                    channel.put(Key.PARSE_BOARD_TITLE,t);
                    channel.put(Key.PARSE_BOARD_DESCRIPTION,desc);
                    progressBar.setVisibility(View.VISIBLE);
                    channel.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if(e==null){
                                channel.pinInBackground(new SaveCallback() {
                                    @Override
                                    public void done(ParseException e) {
                                        if(e==null){
                                            myChannels.reloadBoardsFromLocalDatastore();
                                            dialog.dismiss();
                                        }else{
                                            Toast.makeText(activity,"Unable to save board! Please swipe to refresh.",Toast.LENGTH_SHORT).show();
                                        }
                                        progressBar.setVisibility(View.GONE);
                                    }
                                });
                            }else{
                                Toast.makeText(activity,"Unable to create board! Please try again.",Toast.LENGTH_SHORT).show();
                                progressBar.setVisibility(View.GONE);
                            }
                        }
                    });
                }else{
                    Toast.makeText(activity,error,Toast.LENGTH_SHORT).show();
                }
            }
        });
        DisplayMetrics metrics = activity.getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        int height = metrics.heightPixels;
        dialog.getWindow().setLayout((6 * width)/7, (4 * height)/5);
        return dialog;
    }

}