package com.shadowofsusanoo.ephemeral.adapter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TableRow;
import android.widget.TextView;

import com.parse.Parse;
import com.parse.ParseObject;
import com.shadowofsusanoo.ephemeral.R;
import com.shadowofsusanoo.ephemeral.key.Key;
import com.shadowofsusanoo.ephemeral.utility.Utility;

import java.util.ArrayList;

/**
 * Created by atish on 29/1/16.
 */
public class BoardAdapter extends ArrayAdapter<ParseObject> {

    static final int LAYOUT= R.layout.card_board;
    AppCompatActivity activity;
    ArrayList<ParseObject> list;
    TextView title,description;

    public BoardAdapter(AppCompatActivity activity, ArrayList<ParseObject> list) {
        super(activity, LAYOUT, list);
        this.activity = activity;
        this.list = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View root = inflater.inflate(LAYOUT, parent, false);
        title=(TextView)root.findViewById(R.id.title);
        description=(TextView)root.findViewById(R.id.description);
        title.setText(Utility.cFirst(list.get(position).getString(Key.PARSE_BOARD_TITLE)));
        description.setText(list.get(position).getString(Key.PARSE_BOARD_DESCRIPTION));
        return root;
    }

}
