package com.shadowofsusanoo.ephemeral.adapter;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v7.app.AppCompatActivity;
import com.shadowofsusanoo.ephemeral.fragment.MyChannelsFragment;
import com.shadowofsusanoo.ephemeral.fragment.SearchFragment;

/**
 * Created by atish on 29/1/16.
 */
public class TabsPagerAdapter extends FragmentStatePagerAdapter {

    private static final int TAB_COUNT = 2;
    private AppCompatActivity activity;

    public TabsPagerAdapter(FragmentManager fragmentManager,AppCompatActivity activity) {
        super(fragmentManager);
        this.activity = activity;
    }

    @Override
    public Fragment getItem(int index) {
        switch (index) {
            case 0:
                return MyChannelsFragment.getInstance(activity);
            case 1:
                return SearchFragment.getInstance(activity);
        }
        return null;
    }

    @Override
    public int getCount() {
        return TAB_COUNT;
    }

}
