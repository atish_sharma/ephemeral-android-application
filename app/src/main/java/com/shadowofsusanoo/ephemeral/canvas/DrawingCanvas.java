package com.shadowofsusanoo.ephemeral.canvas;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Path;
import com.shadowofsusanoo.ephemeral.model.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import com.firebase.client.DataSnapshot;
import com.firebase.client.Firebase;
import com.firebase.client.FirebaseError;
import com.firebase.client.ValueEventListener;
import com.shadowofsusanoo.ephemeral.R;

/**
 * Created by atish on 30/1/16.
 */
public class DrawingCanvas extends View {

    Context context;
    Paint paint;
    float touched_x=0,touched_y=0;
    Path path;
    Firebase firebase;
    static final String HASH="_";
    String boardId;
    int w,h;

    public DrawingCanvas(Context context,AttributeSet attr){
        super(context,attr);
        this.context=context;
        path=new Path();
        paint=new Paint();
        paint.setAntiAlias(true);
        paint.setStyle(Paint.Style.STROKE);
        paint.setStrokeWidth(4f);
        paint.setColor(context.getResources().getColor(R.color.write));
        path.reset();
    }

    public void setFirebaseReference(final String id, final String s){
        firebase=new Firebase(s);
        boardId=id;
        firebase.child(id).addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {
                String val="";
                try {
                    val = dataSnapshot.getValue().toString();
                }catch(Exception e){
                    e.printStackTrace();
                    return;
                }
                String[] t=val.split(HASH);
                if(t.length==3){
                    try {
                        float x = Float.parseFloat(t[0]);
                        float y = Float.parseFloat(t[1]);
                        String action = t[2];
                        Point p=new Point(x,y);
                        p=p.convertToStandard(p,1024,768,w,h);
                        x=p.getX();
                        y=p.getY();
                        if (action.equals("down")) {
                            path.moveTo(x, y);
                            postInvalidate();
                        } else if (action.equals("move")) {
                            path.lineTo(x, y);
                            postInvalidate();
                        }else if(action.equals("clear")){
                            path.reset();
                            postInvalidate();
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onCancelled(FirebaseError firebaseError) {}

        });
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        this.w = w;
        this.h = h;
    }

    void sendValue(String x,String y,String action){
        firebase.child(boardId).setValue(x + HASH + y + HASH + action);
    }

    @Override
    protected void onDraw(Canvas canvas){
        super.onDraw(canvas);
        canvas.drawPath(path, paint);
    }

    public void clearCanvas(){
        sendValue("0","0","clear");
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        super.onTouchEvent(event);
        touched_x = event.getX();
        touched_y = event.getY();
        int action = event.getAction();
        switch (action) {
            case MotionEvent.ACTION_DOWN:
                Point p=new Point(touched_x,touched_y);
                p=p.convertToStandard(p,w,h,1024,768);
                sendValue(String.valueOf(p.getX()),String.valueOf(p.getY()),"down");
                break;
            case MotionEvent.ACTION_MOVE:
                Point p2=new Point(touched_x,touched_y);
                p2=p2.convertToStandard(p2,w,h,1024,768);
                sendValue(String.valueOf(p2.getX()), String.valueOf(p2.getY()), "move");
                break;
            default:
                return false;
        }
        return true;
    }

}
